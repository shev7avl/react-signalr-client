using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.WebHost.Clients;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{
    public class ChatHub: Hub<IChatClient>
    {
        public async Task SendMessage(ChatMessage message)
        {
            await Clients.All.ReceiveMessage(message);
            }
    }
}