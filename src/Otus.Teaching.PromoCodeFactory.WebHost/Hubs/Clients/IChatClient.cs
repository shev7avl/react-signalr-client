using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Hubs;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Clients
{
    public interface IChatClient
    {
        Task ReceiveMessage(ChatMessage message);
    }

}