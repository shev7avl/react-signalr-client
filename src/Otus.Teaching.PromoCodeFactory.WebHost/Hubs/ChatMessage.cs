namespace Otus.Teaching.PromoCodeFactory.WebHost.Hubs
{
    public class ChatMessage
    {
        public string User { get; set; }

        public string Message {get; set; }
    }
}