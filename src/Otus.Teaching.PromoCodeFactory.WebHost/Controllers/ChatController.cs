using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Otus.Teaching.PromoCodeFactory.WebHost.Clients;
using Otus.Teaching.PromoCodeFactory.WebHost.Hubs;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ChatController : ControllerBase
    {
        private readonly IHubContext<ChatHub, IChatClient> _context;
        public ChatController(IHubContext<ChatHub, IChatClient> context){
    
        _context = context;

}
    [HttpPost]
    public async Task PostMessage(ChatMessage message) {

        message.Message = $"Posted to ChatController: {message.Message}";
        await _context.Clients.All.ReceiveMessage(message);
    }

    }

}